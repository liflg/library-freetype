Website
=======
https://www.freetype.org/

License
=======
FreeType License or GPL version 2 (see the file source/docs/LICENSE.TXT)

Version
=======
2.9.1

Source
======
freetype-2.9.1.tar.bz2 (sha256: db8d87ea720ea9d5edc5388fc7a0497bb11ba9fe972245e0f7f4c7e8b1e1e84d)

Required by
===========
* CEGUI
* FTGL
* Ogre10
* SDL_ttf
